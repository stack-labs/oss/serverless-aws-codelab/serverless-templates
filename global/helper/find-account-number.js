const AWS = require("aws-sdk");

module.exports = sls => {
  const sts = new AWS.STS({ region: sls.service.provider.region });
  return sts
    .getCallerIdentity()
    .promise()
    .then(identity => {
      // console.log(identity);
      return identity.Account
    })
    .catch(err => console.log(err));
};
