const uuidv1 = require("uuid/v1");

module.exports = () => {
  return uuidv1().split("-")[0]; 
};
