import React from 'react';

import { storiesOf } from '@storybook/react'; // eslint-disable-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions'; // eslint-disable-line import/no-extraneous-dependencies
// import { linkTo } from '@storybook/addon-links';

import EndpointCard from '../src/components/EndpointCard/EndpointCard.component';
import ResultCard from '../src/components/ResultCard/ResultCard.component';
import Logo from '../src/components/Logo/Logo.component';
import SelectImageButton from '../src/components/SelectImageButton/SelectImageButton.component';

storiesOf('EndpointCard', module)
  .add('standard', () => (
    <EndpointCard
      handleChange={action('handleChange')}
      endpointUrl=""
    />
  ));

storiesOf('ResultCard', module)
  .add('standard', () => (
    <ResultCard
      handleChange={action('handleChange')}
      name="evil_cat.png"
      probability="99"
      result="cat"
      imgUrl="https://news.nationalgeographic.com/content/dam/news/2018/05/17/you-can-train-your-cat/02-cat-training-NationalGeographic_1484324.ngsversion.1526587209178.adapt.1900.1.jpg"
    />
  ));

storiesOf('Icon', module)
  .add('standard', () => (
    <Logo />
  ))
  .add('on drag', () => (
    <Logo onDrag />
  ));

storiesOf('Button', module)
  .add('standard', () => (
    <SelectImageButton>Select an image</SelectImageButton>
  ));
