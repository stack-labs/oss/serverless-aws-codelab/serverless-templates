import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const styles = {
  card: {
    border: '1px solid #707070',
    borderRadius: 25,
    fontFamily: 'Helvetica Neue',
    fontSize: 20,
    color: '#4A4A4A',
    padding: 12,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  description: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '0 10px',
    width: '100%',
    textAlign: 'center',
  },
  imageContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  image: {
    maxWidth: 128,
    borderRadius: 25,
  },
};

const dateOptions = {
  year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric',
};

const ResultCard = ({
  name, result, confidence, imgUrl, style, timestamp,
}) => (
  <div style={{ ...style, ...styles.card }}>
    <div style={styles.imageContainer}>
      <img
        style={styles.image}
        alt={name}
        src={imgUrl}
      />
    </div>
    <div style={styles.description}>
      <div>{new Date(timestamp).toLocaleDateString('fr-FR', dateOptions)}</div>
      <div>{name}</div>
      <br />
      <div>{result}</div>
      <div>
        {confidence}
        %
      </div>
    </div>
  </div>
);


ResultCard.defaultProps = {
  imgUrl: '',
  timestamp: '',
};

ResultCard.propTypes = {
  name: PropTypes.string.isRequired,
  result: PropTypes.string.isRequired,
  confidence: PropTypes.number.isRequired,
  imgUrl: PropTypes.string,
  timestamp: PropTypes.string,
  style: PropTypes.object, // eslint-disable-line
};

export default withStyles(styles)(ResultCard);
