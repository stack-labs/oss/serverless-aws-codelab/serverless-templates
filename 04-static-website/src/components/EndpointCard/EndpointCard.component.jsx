import React from 'react';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const styles = {
  card: {
    fontFamily: 'Helvetica Neue',
    fontSize: 30,
    display: 'flex',
    flexDirection: 'column',
    minWidth: 360,
  },
  textField: {
    borderRadius: 25,
    fontSize: 30,
  },
  container: {
    padding: 24,
    border: '1px solid #707070',
    borderRadius: 25,
  },
};

const EndpointCard = ({ handleChange, endpointUrl }) => (
  <div style={styles.card}>
    <div style={styles.container}>
      <div>Your API Endpoint</div>
      <TextField
        id="outlined-endpoint-url"
        value={endpointUrl}
        onChange={handleChange}
        margin="normal"
        placeholder="endpoint url"
        variant="outlined"
        fullWidth
      />
    </div>
  </div>
);

EndpointCard.propTypes = {
  // classes: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  handleChange: PropTypes.func.isRequired,
  endpointUrl: PropTypes.string.isRequired,
};

export default withStyles(styles)(EndpointCard);
