import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  button: {
    border: '1px solid #707070',
    textTransform: 'capitalize',
  },
});

class ButtonComponent extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    this.form = React.createRef();
  }

  refresh() {
    const { refresh } = this.props;
    refresh();
  }

  render() {
    const { classes, children, style } = this.props;
    return (
      <div>
        <Button
          className={classes.button}
          onClick={() => this.refresh()}
          style={style}
        >
          {children}
        </Button>
      </div>
    );
  }
}

ButtonComponent.propTypes = {
  classes: PropTypes.object.isRequired, // eslint-disable-line
  children: PropTypes.string.isRequired,
  style: PropTypes.object.isRequired, // eslint-disable-line
  refresh: PropTypes.func.isRequired,
};

export default withStyles(styles)(ButtonComponent);
