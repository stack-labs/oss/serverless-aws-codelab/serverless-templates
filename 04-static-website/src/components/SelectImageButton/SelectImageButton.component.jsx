import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  button: {
    border: '1px solid #707070',
    textTransform: 'capitalize',
  },
});

const buildFilename = (filename) => {
  const rand = Math.floor(Math.random() * 1000);
  const [extension, ...filenameParts] = filename.split('.').reverse();
  return `${filenameParts.join('.').replace(/[ \(\)]/g, '_')}_${rand}.${extension}`; // eslint-disable-line no-useless-escape
};

class ButtonComponent extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    this.form = React.createRef();
  }

  async uploadFile() {
    const formData = new FormData(); // eslint-disable-line no-undef
    const { showSnackbar, endpoint } = this.props;
    const filename = buildFilename(this.textInput.current.files[0].name);
    showSnackbar(`Uploading file ${filename}`);
    const s3up = await fetch(`${endpoint}?filename=${filename}`, {
      method: 'get',
    });
    const s = await s3up.json();
    Object.keys(s.fields).forEach((k) => {
      formData.append(k, s.fields[k]);
    });
    formData.append('file', this.textInput.current.files[0]);
    try {
      await fetch(s.url, {
        method: 'post',
        body: formData,
      });
      showSnackbar(`File ${filename} is uploaded`);
      showSnackbar('Wait a few seconds before refreshing to see your thumbnail');
    } catch (e) {
      showSnackbar(`Failed to upload file ${filename}`);
      console.error(e); // eslint-disable-line no-console
    }
  }

  openFileDialog() {
    this.textInput.current.click();
  }

  render() {
    const { classes, children, style } = this.props;
    return (
      <div>
        <Button
          className={classes.button}
          onClick={() => this.openFileDialog()}
          style={style}
        >
          {children}
        </Button>
        <form
          ref={this.form}
          action="."
          encType="multipart/form-data"
        >
          <input
            ref={this.textInput}
            type="file"
            style={{ display: 'none' }}
            onChange={() => this.uploadFile()}
          />
        </form>
      </div>
    );
  }
}

ButtonComponent.defaultProps = {
  endpoint: '',
};

ButtonComponent.propTypes = {
  classes: PropTypes.object.isRequired, // eslint-disable-line
  children: PropTypes.string.isRequired,
  endpoint: PropTypes.string,
  style: PropTypes.object.isRequired, // eslint-disable-line
  showSnackbar: PropTypes.func.isRequired,
};

export default withStyles(styles)(ButtonComponent);
