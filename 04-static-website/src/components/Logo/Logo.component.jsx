import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import logo from '../../assets/logo_small.jpg';

const styles = () => ({
  logoContainer: {
    width: 200,
    height: 200,
    display: 'flex',
  },
  logo: {
    borderRadius: '50%',
    border: '1px solid black',
    maxWidth: 198,
  },
  logoOnDrag: {
    borderStyle: 'dashed',
  },
});

const Header = ({ classes, onDrag }) => (
  <div className={classes.logoContainer}>
    <img src={logo} alt="stack-labs-logo" className={`${classes.logo} ${onDrag && classes.logoOnDrag}`} />
  </div>
);

Header.defaultProps = {
  onDrag: false,
};

Header.propTypes = {
  classes: PropTypes.object.isRequired, // eslint-disable-line
  onDrag: PropTypes.bool,
};

export default withStyles(styles)(Header);
