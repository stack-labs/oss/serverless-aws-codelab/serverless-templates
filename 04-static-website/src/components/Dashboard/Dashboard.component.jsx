import React, { useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Media from 'react-media';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';

import LogoComponent from '../Logo/Logo.component';
import SelectImageButtonComponent from '../SelectImageButton/SelectImageButton.component';
import RefreshButtonComponent from '../RefreshButton/RefreshButton.component';
import ResultCardComponent from '../ResultCard/ResultCard.component';

const styles = () => ({
  root: {
    flexGrow: 1,
    padding: '60px 10px 20px 10px',
  },
  uploadContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    height: 300,
    alignItems: 'center',
  },
  dragAndUpload: {
    fontSize: 30,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  picsCardsContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: 10,
  },
  logo: {
  },
});

function Dashboard(props) {
  const [snackbar, setSnackbar] = useState({
    open: false,
    message: '',
    action: [],
  });
  const [images, setImages] = useState([]);
  const [config, setConfig] = useState({});
  const { classes } = props;

  const handleClose = () => {
    setSnackbar({
      ...snackbar,
      open: false,
    });
  };

  const showSnackbar = (message, getAction = () => []) => {
    setSnackbar({
      ...snackbar,
      open: true,
      message,
      action: getAction(handleClose),
    });
  };

  const refresh = c => () => {
    showSnackbar('refreshing');
    setImages([]);
    fetch(c.api.resultEndpoint)
      .then(results => results.json())
      .then(setImages)
      .then(() => showSnackbar('dashboard up-to-date'))
      .catch((e) => {
        showSnackbar('Failed to refresh');
        setImages([]);
        console.error(e); // eslint-disable-line no-console
      });
  };

  const loadConfig = () => {
    fetch('/config.json')
      .then(results => results.json())
      .then((c) => {
        setConfig(c);
        return c;
      })
      .then(c => refresh(c)());
  };

  React.useEffect(loadConfig, []);

  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={12} className={classes.uploadContainer}>
          <Media query={{ minWidth: 799 }}>
            {(matches) => { // eslint-disable-line arrow-body-style
              return matches ? (<LogoComponent />) : null;
            }}
          </Media>
          <div className={classes.dragAndUpload}>
            <SelectImageButtonComponent
              showSnackbar={showSnackbar}
              endpoint={config.api && config.api.preSignedEndpoint}
              style={{ fontSize: 30 }}
              onRefreshSuccess
            >
              Select An Image
            </SelectImageButtonComponent>
          </div>
        </Grid>
        <RefreshButtonComponent
          refresh={refresh(config)}
          style={{ fontSize: 15, margin: 20 }}
          onRefreshSuccess
        >
          Refresh dashboard
        </RefreshButtonComponent>
        <Grid className={classes.picsCardsContainer}>
          {images.sort((r1, r2) => new Date(r2.timestamp) - new Date(r1.timestamp)).map(r => (
            <ResultCardComponent
              key={r.id}
              name={r.name}
              result={r.result}
              confidence={Math.round(r.confidence * 1000) / 10}
              imgUrl={`${config.bucket.thumbnail}/${r.name}`}
              style={{ margin: 10 }}
              timestamp={r.timestamp}
            />
          ))}
        </Grid>
      </Grid>
      <Snackbar
        open={snackbar.open}
        onClose={handleClose}
        ContentProps={{
          'aria-describedby': 'posts-refreshed',
        }}
        autoHideDuration={3000}
      >
        <SnackbarContent
          message={<span id="posts-refreshed">{snackbar.message}</span>}
          action={snackbar.action}
        />
      </Snackbar>
    </div>
  );
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired, // eslint-disable-line
};

export default withStyles(styles)(Dashboard);
